/**
 * @file gpio.h
 * @brief Contains all functions definition in gpio.c
 * 
 * @author Žygimantas Kungelis
 */ 

#ifndef GPIO_HEADER
#define GPIO_HEADER

#include "main.h"


/**
 * @brief Enums used only in gpio_table as indexes for clearer code.
 * 
 * @param eGPIO_OC_PWM_TIM3 for PWM output channel pin PC7;
 * @param eGPIO_LD2 for LED pin PA5.
 * @param eGPIO_IC_TIM2 for input counter pin PA0;
 * @param GPIO_MAX the number of initialised GPIOs.
 */
enum GPIO_NAMES {
    eGPIO_OC_PWM_TIM3 = 0,
    eGPIO_LD2,
    eGPIO_IC_TIM2,
    GPIO_MAX
};

/**
 * @brief A struct for GPIO initialisation.
 * 
 * More detailed explanation:
 * A struct containing all needed information for initialisation of needed
 * GPIOs.
 * 
 * Note: it is not expected for the information in the table to change
 * during run-time.
 * 
 * @param Pin specifies the GPIO pins to be configured;
 * @param Mode specifies the operating mode for the selected pins;
 * @param Alternate peripheral to be connected to the selected pins;
 * @param Pull specifies the Pull-up or Pull-Down activation for the selected 
 *  pins;
 * @param Speed specifies the speed for the selected pins;
 * @param Port describes which port this pin belongs.
 */ 
struct GPIO {
    const uint32_t Pin;
    const uint32_t Mode;
    const uint32_t Alternate;
    const uint32_t Pull;
    const uint32_t Speed;
    GPIO_TypeDef * const Port;
} gpio_table[] = {
    [eGPIO_OC_PWM_TIM3] = {GPIO_PIN_7, GPIO_MODE_AF_PP, GPIO_AF2_TIM3, 
                          GPIO_NOPULL, GPIO_SPEED_FREQ_LOW, GPIOC},
    [eGPIO_LD2] = {LED_PIN, GPIO_MODE_OUTPUT_PP, 0, GPIO_NOPULL, 
                   GPIO_SPEED_FREQ_VERY_HIGH, LED_PORT},
    [eGPIO_IC_TIM2] = {GPIO_PIN_0, GPIO_MODE_AF_PP, GPIO_AF1_TIM2, GPIO_NOPULL,
                    GPIO_SPEED_FREQ_LOW, GPIOA}
};



// Function declarations



/**
 * @brief  GPIO initialisation according to gpio_table values.
 * 
 * Detailed description starts here:
 * This function enables port clocks, initialises pins and enables NVIC.
 * 
 * @return Void
 */
extern void GPIO_Init(void);

/**
 * @brief  Enables clocks for ports A, B, C and H.
 * 
 * Detailed description starts here:
 * Enables needed ports' clocks using __HAL_RCC_GPIOx_CLK_ENABLE().
 * 
 * @return Void
 */
static void enable_GPIOx_Clocks(void);

/**
 * @brief Cleans a given handler
 * 
 * Detailed description starts here:
 * Nulls all values in the given pointer.
 * 
 * @param specificHandler a pointer to a specific handler, which needs 
 * cleaning.
 * @returns Void.
 */ 
static void clean_handler(void *specificHandler);

/**
 * @brief  GPIO initialisation without enabling NVIC.
 * 
 * Detailed description starts here:
 * This function only enables initialises pins.
 * 
 * @param specificHandler a specific gpio initTypeDef handler;
 * @param table_value a specific value from gpio_table.
 * @return Void
 */
static void init_GPIO_without_NVIC(
                        GPIO_InitTypeDef * const specificHandler, 
                        const struct GPIO * const table_value);

/**
 * @brief  GPIO initialisation without enabling NVIC.
 * 
 * Detailed description starts here:
 * This function only enables initialises pins.
 * 
 * @param specificHandler a specific gpio initTypeDef handler;
 * @param table_value a specific value from gpio_table;
 * @param IRQn_handler_name which IRQn to enable;
 * @param PreemPriority preemt-priority for IRQn;
 * @param SubPriority sub-priority for IRQn.
 * @return Void
 */
static void init_GPIO_with_NVIC(
                        GPIO_InitTypeDef * const specificHandler, 
                        const struct GPIO * const table_value,
                        const IRQn_Type IRQn_handler_name,
                        const uint32_t PreemtPriority,
                        const uint32_t SubPriority);

/**
 * @brief  General GPIO initialisation function.
 * 
 * Detailed description starts here:
 * Initialises GPIO using the given handler and the infomation
 * in a specific gpio_table value.
 * 
 * @param  specificHandler A specific handler, options are found in
 * main.h.
 * @param  gpio_table_value A value from gpio_table structure, 
 * found in hardware_config_functions.h.
 * @return Void
 */
static void init_specific_GPIO(GPIO_InitTypeDef * const specificHandler, 
                               const struct GPIO * const gpio_table_value);

/**
 * @brief Handles init errors.
 * 
 * Detailed description starts here:
 * Declaration is in handlers.c file.
 * 
 * @return Void.
 */ 
extern void Error_handler(void);

#endif // GPIO_HEADER