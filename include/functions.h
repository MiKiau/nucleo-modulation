/**
 * @file functions.h
 * @brief Contains all functions definitions of functions used in functions.c
 * 
 * This file contains all definitions of functions used for
 * pulse receival, pulse modification.
 * 
 * @author Žygimantas Kungelis
 */ 

#ifndef FUNCTIONS
#define FUNCTIONS

#include "main.h"



#define PI                          3.14
#define GOLDEN_RATIO                1.618

#define TIM3_Prescaler              72
#define System_Clock                72000000UL



/**
 * @brief Calculates frequency according to task number.
 * 
 * Detailed description starts here:
 * Currently, there are 6 tasks. So, this function will return a calculated 
 * frequency using given frequency and specific formulas set here. If the 
 * taskNumber is bigger than 6, then the return value is always the value 
 * of frequency.
 * 
 * @param taskNumber the number of task currently active;
 * @param frequency the frequency calculated from received pulse.
 * @return Frequency generated according to specific formula from specific 
 *  task.
 */ 
extern double frequencyAccordingToTask(const int taskNumber, 
                                       const uint32_t frequency);

/**
 * @brief Changes TIM3 pulse's frequency and duty cycle.
 * 
 * Detailed description starts here:
 * Changes TIM3 channel 2 ARR and CCR2 values using formulas:
 * ARR = F(SYS_CLK) / ( (PSC+1) * F(PWM) ) - 1;
 * CCR2 = ARR * DutyCycle[%] / 100[%]. 
 * 
 * @param frequency the frequency of the wanted pulse;
 * @param dutyCycle the percentage of the wanted pulse's duty cycle.
 * @return Void.
 */ 
extern void changeFrequencyAndDutyCycle(const uint32_t frequency, 
                                        const float dutyCycle);

/**
 * @brief Calculates absolute value of two uint32 variables substraction. 
 * 
 * @param a a uint32 variable;
 * @param b a uint32 variable.
 * @return The result of |a-b|.
 */ 
extern uint32_t absForUint(const uint32_t a, const uint32_t b);

/**
 * @brief Finds the most frequent element in the uint32_t array
 * 
 * @param frequencyArray array containing uint32_t elements;
 * @param size the size of the given array.
 * @return the most frequent element's value.
 */ 
extern uint32_t mostFrequent(uint32_t *frequencyArray, int size);

/**
 * @brief A comparison function for uint32_t elements
 * 
 * Detailed description start here:
 * A comparison function used in mostFrequent function's qsort function.
 * 
 * @param elem1 the first element;
 * @param elem2 the sencond element.
 * @return 1 if elem1 is bigger, 0 otherwise.
 */ 
static int comp(const void *elem1, const void *elem2);

#endif