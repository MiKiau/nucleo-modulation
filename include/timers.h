/**
 * @file timers.h
 * @brief Contains all functions definition in timers.c
 * 
 * This file contains enums (TIMER_NAMES and IOC_CHANNEL_CONFIG), structs
 * (TIMER, PULSE_OC_CONFIG and PULSE_IC_CONFIG) and funtions headers all
 * related to timers.
 * 
 * @author Žygimantas Kungelis
 */ 


#ifndef TIMERS_HEADER
#define TIMERS_HEADER


#include "main.h"


/**
 * @brief Enums used only in timer_table as indexes.
 * 
 * @param ePWM_TIM3 for PWM timer #3;
 * @param eInput_TIM2 for IC timer #2;
 * @param Timer_MAX the number of configurated timers.
 */
enum TIMER_NAMES {
    ePWM_TIM3 = 0,
    eInput_TIM2,
    eGeneric_TIM16,
    Timer_MAX
};

/**
 * @brief A struct for GPIO initialisation.
 * 
 * More detailed explanation:
 * A struct containing all needed information for initialisation of LD2,
 * UART1 and UART2.
 * 
 * Note: it is not expected for the information in the table to change
 * during run-time.
 * 
 * @param Instance specifies, which timer to configurate;
 * @param Prescaler specifies the prescaler value used to divide the TIM clock;
 * @param CounterMode specifies the counter mode;
 * @param Period the maximum number until which the timer counts;
 * @param ClockDivision specifies the clock division;
 * @param AutoReload specifies the auto reload value to be loaded into the 
 *  active Auto-Reload Register at the next update event.
 */
struct TIMER {
    TIM_TypeDef * const Instance;
    const uint32_t Prescaler;
    const uint32_t CounterMode;
    const uint32_t Period;
    const uint32_t ClockDivision;
    const uint32_t AutoReloadPreload;
    const uint32_t ClockSource;
} timer_table[] = {
    [ePWM_TIM3] = {TIM3, 72 - 1, TIM_COUNTERMODE_UP, 65536 - 1, 
                     TIM_CLOCKDIVISION_DIV1, TIM_AUTORELOAD_PRELOAD_ENABLE,
                     TIM_CLOCKSOURCE_INTERNAL},
    [eInput_TIM2] = {TIM2, 36 - 1, TIM_COUNTERMODE_UP, 65536 - 1, 
                     TIM_CLOCKDIVISION_DIV1, TIM_AUTORELOAD_PRELOAD_ENABLE,
                     TIM_CLOCKSOURCE_INTERNAL}
};

/**
 * @brief Enums used only in PWM_config_table as indexes.
 * 
 * @param eOUTPUT1 for the first output PWM channel;
 * @param eINPUT1 for the first input IC channel.
 * @param IOC_CONFIG_MAX the number of configurated IOC (input/output counter)
 *  channels.
 */
enum IOC_CHANNEL_CONFIG {
    eOUTPUT1 = 0,
    eINPUT1,
    IOC_CONFIG_MAX
};

/**
 * @brief A struct for PWM configuration.
 * 
 * Note: it is not expected for the information in the table to change
 * during run-time.
 * 
 * @param OCMode specifies the output mode;
 * @param Pulse specifies the pulse value to be loaded into the Capture 
 *  Compare Register;
 * @param OCPolarity specifies the output polarity;
 * @param OCFastMode specifies the Fast mode state;
 * @param Channel describes which channel should be configurated;
 */
struct PULSE_OC_CONFIG {
    const uint32_t OCMode;
    const uint32_t Pulse;
    const uint32_t OCPolarity;
    const uint32_t OCFastMode;
    const uint32_t Channel;
} OC_channel_config[] = {
    [eOUTPUT1] = {TIM_OCMODE_PWM1, 0, TIM_OCPOLARITY_HIGH, TIM_OCFAST_DISABLE,
                  TIM_OUTPUT_CHANNEL}
};

/**
 * @brief A struct for PWM configuration.
 * 
 * Note: it is not expected for the information in the table to change
 * during run-time.
 * 
 * @param ICPolarity specifies the active edge of the input signal;
 * @param ICSelection specifies the input;
 * @param ICPrescaler specifies the Input Capture Prescaler;
 * @param ICFilter specifies the input capture filter;
 * @param Channel describes which channel should be configurated;
 */
struct PULSE_IC_CONFIG {
    const uint32_t ICPolarity;
    const uint32_t ICSelection;
    const uint32_t ICPrescaler;
    const uint32_t ICFilter;
    const uint32_t Channel;
} IC_channel_config[] = {
    [eINPUT1] = {TIM_INPUTCHANNELPOLARITY_RISING, TIM_ICSELECTION_DIRECTTI, 
                 TIM_ICPSC_DIV1, 0, TIM_INPUT_CHANNEL}
};



/**
 * @brief  System Clock Configuration
 * 
 * Detailed description starts here:
 * The system Clock is configured as follow :
 *  System Clock source            = MSI
 *  SYSCLK(MHz)                    = 72
 *  HCLK(MHz)                      = 72
 *  AHB Prescaler                  = 1
 *  APB1 Prescaler                 = 1
 *  APB2 Prescaler                 = 1
 * 
 * @return Void
 */
extern void SystemClock_Config(void);

/**
 * @brief  Initialisation and configuration of all timers
 * 
 * Detailed description starts here:
 * This function initialises and configurates:
 *  - TIM2 as input counter;
 *  - TIM3 as output counter, PWM.
 * 
 * @return Void
 */
extern void TIM_Init(void);

/**
 * @brief Enables timers.
 * 
 * Detailed description starts here:
 * Enables TIM2 and TIM3 using __HAL_RCC_TIMx_CLK_ENABLE() macro.
 * 
 * @returns Void.
 */ 
static void enable_TIMx_clocks(void);

/**
 * @brief Cleans a given handler
 * 
 * Detailed description starts here:
 * Nulls all values in the given pointer.
 * 
 * @param specificHandler a pointer to a specific handler, which needs 
 * cleaning.
 * @returns Void.
 */ 
static void clean_handler(void *specificHandler);

/**
 * @brief Initialise and configurate a timer as OC, PWM.
 * 
 * Detailed description starts here:
 * Initialise specific timer according to it's timer_table value and
 * configurates it as OC, PWM according to OC_channel_config table value.
 * 
 * @param specificHandler a specific timer handle type struct;
 * @param time_table_value a specific value from time_table struct;
 * @param OC_config a specific timer OC init type struct;
 * @param OC_table_value a specific OC_channel_config table value;
 * @returns Void.
 */
static void init_timer_as_PWM(TIM_HandleTypeDef * const specificHandler,
                        const struct TIMER * const time_table_value,
                        TIM_OC_InitTypeDef * const OC_config,
                        const struct PULSE_OC_CONFIG * const OC_table_value); 

/**
 * @brief Initialise and configurate a timer as IC.
 * 
 * Detailed description starts here:
 * Initialise specific timer according to it's timer_table value and
 * configurates it as IC according to IC_channel_config table value.
 * 
 * @param specificHandler a specific timer handle type struct;
 * @param time_table_value a specific value from time_table struct;
 * @param IC_config a specific timer IC init type struct;
 * @param IC_table_value a specific IC_channel_config table value;
 * @returns Void.
 */
static void init_timer_as_IC(TIM_HandleTypeDef * const specificHandler,
                        const struct TIMER * const time_table_value,
                        TIM_IC_InitTypeDef * const IC_config,
                        const struct PULSE_IC_CONFIG * const IC_table_value); 

/**
 * @brief  General timer initialisation function.
 * 
 * Detailed description starts here:
 * Initialises timer (TIM) using the given handler and the infomation
 * in a specific timer_table value.
 * 
 * @param  specificHandler A specific handler, options are found in
 * main.h.
 * @param  timer_table_value A value from timer_table structure, 
 * found in timers.h.
 * @return Void
 */
static void init_specific_Timer(TIM_HandleTypeDef * const specificHandler, 
                                const struct TIMER * const timer_table_value);

/**
 * @brief  A general PWM channel initialisation function.
 * 
 * Detailed description starts here:
 * Initialises and configurates PWM channel according to
 * a specific value in OC_table_value.
 * 
 * @param  TIM_handler A specific handler, options are found in
 * main.h.
 * @param  OC_config A specific TIM_OC_InitTypeDef structure.
 * @param  table_value A value from OC_table_value structure, 
 * found in timers.h.
 * @return Void
 */
static void init_OC_channel(TIM_HandleTypeDef * const TIM_handler,
                            TIM_OC_InitTypeDef * const OC_config,
                            const struct PULSE_OC_CONFIG * const table_value);


/**
 * @brief  A general IC channel initialisation function.
 * 
 * Detailed description starts here:
 * Initialises and configurates IC channel according to
 * a specific value in IC_table_value.
 * 
 * @param  TIM_handler A specific handler, options are found in
 * main.h.
 * @param  IC_config A specific TIM_IC_InitTypeDef structure.
 * @param  table_value A value from IC_table_value structure, 
 * found in timers.h.
 * @return Void
 */
static void init_IC_channel(TIM_HandleTypeDef * const TIM_handler,
                            TIM_IC_InitTypeDef * const IC_config,
                            const struct PULSE_IC_CONFIG * const table_value);

/**
 * @brief Handles init errors.
 * 
 * Detailed description starts here:
 * Declaration is in handlers.c file.
 * 
 * @return Void.
 */ 
extern void Error_handler(void);

#endif // TIMERS_HEADER