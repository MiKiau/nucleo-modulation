/**
 * @file main.h
 * @brief All includes, global variables and functions for main.c are here.
 * 
 * @section DESCRIPTION
 * 
 * This file contains elements used in main.c, such as:
 * 1. Included files;
 * 2. Global variables;
 * 3. Macros;
 * 4. Function definitions.
 * 
 * @author Žygimantas Kungelis
 */ 



#ifndef MAIN
#define MAIN



// Included files
#include "stm32l4xx_hal.h"
#include <math.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>


// LED pin for quick access
#define LED_PIN             GPIO_PIN_5
// LED port for quick access
#define LED_PORT            GPIOA 


// PWM TIM channel
#define TIM_OUTPUT_CHANNEL  TIM_CHANNEL_2
// IC TIM channel
#define TIM_INPUT_CHANNEL   TIM_CHANNEL_1


// GPIO initialisitaion handlers
GPIO_InitTypeDef hPWMoutput, hTIM2gpio, hled;
// Timers initialisation handlers
TIM_HandleTypeDef htim2, htim3;
// Timer's output counter configuration structure
TIM_OC_InitTypeDef outputCounterConfig;
// Timer's input counter configuration structure
TIM_IC_InitTypeDef inputCounterConfig;


/**
 * @brief  System Clock Configuration
 * 
 * The system Clock is configured as follow :
 *  System Clock source            = MSI
 *  SYSCLK(MHz)                    = 72
 *  HCLK(MHz)                      = 72
 *  AHB Prescaler                  = 1
 *  APB1 Prescaler                 = 1
 *  APB2 Prescaler                 = 1
 * 
 * @return Void
 */
extern void SystemClock_Config(void);

/**
 * @brief  GPIO initialisation according to gpio_table values.
 * 
 * Detailed description starts here:
 * This function enables port clocks (GPIOA, GPIOB, GPIOC, GPIOH), intiliases
 * pins (PA5, PA9, PC7) and enables NVIC for pin PC7.
 * 
 * @return Void
 */
extern void GPIO_Init(void);

/**
 * @brief  Timers initialisation according to timer_table
 * 
 * Detailed description starts here:
 * This function initialises timer 3 (TIM3) and timer 16 (TIM16). Also,
 * timer 3 is configurated for PWM channel.
 * 
 * @return Void
 */
extern void TIM_Init(void);

/**
 * @brief Calculates frequency according to task number.
 * 
 * Detailed description starts here:
 * Currently, there are 6 tasks. So, this function will return a calculated 
 * frequency using given frequency and specific formulas set here. If the 
 * taskNumber is bigger than 6, then the return value is always the value 
 * of frequency.
 * 
 * @param taskNumber the number of task currently active;
 * @param frequency the frequency calculated from received pulse.
 * @return Frequency generated according to specific formula from specific 
 *  task.
 */ 
extern double frequencyAccordingToTask(const int taskNumber, 
                                       const uint32_t frequency);

/**
 * @brief Changes TIM3 pulse's frequency and duty cycle.
 * 
 * Detailed description starts here:
 * Changes TIM3 channel 2 ARR and CCR2 values using formulas:
 * ARR = F(SYS_CLK) / ( (PSC+1) * F(PWM) ) - 1;
 * CCR2 = ARR * DutyCycle[%] / 100[%]. 
 * 
 * @param frequency the frequency of the wanted pulse;
 * @param dutyCycle the percentage of the wanted pulse's duty cycle.
 * @return Void.
 */ 
extern void changeFrequencyAndDutyCycle(const uint32_t frequency, 
                                        const float dutyCycle);

/**
 * @brief Calculates absolute value of two uint32 variables substraction. 
 * 
 * @param a a uint32 variable;
 * @param b a uint32 variable.
 * @return The result of |a-b|.
 */ 
extern uint32_t absForUint(const uint32_t a, const uint32_t b);

/**
 * @brief Finds the most frequent element in the uint32_t array
 * 
 * @param frequencyArray array containing uint32_t elements;
 * @param size the size of the given array.
 * @return the most frequent element's value.
 */ 
extern uint32_t mostFrequent(uint32_t *frequencyArray, int size);

#endif // MAIN