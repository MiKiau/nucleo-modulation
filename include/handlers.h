/**
 * @file handlers.h
 * @brief Contains all functions definition in handlers.c
 * 
 * @author Žygimantas Kungelis
 */ 


#include "main.h"


extern void Error_handler(void);
extern void TIM2_IRQHandler(void);
extern void TIM3_IRQHandler(void);
extern void SysTick_Handler(void);