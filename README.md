# Pulse modulation 0.2v - FINAL

A basic C program with STM32 HAL library. It's main purpose: to improve coding ability and to learn more about embedded programing.

## Description

The final program must do:
1. Receive a pulse signal and analyse it's frequency;
2. Modify the pulse frequency according to specific formulas;
3. Send the the pulse with new frequency, but the same duty cycle, back to the source.

## Requirements

Requirements needed for this program to function properly:
1. Nucleo STM32L476RG board for the main program.
2. A pulse sending device (the testing device was Arduino Nano);
3. BreadBoard (if the connection will be made using it);
4. Jumper Wires x3 (wire endings depend on receiving device and the method of connecting boards. The program uses female D8 and D9 Nucleo slots, so one wire ending must be of male type (slot in));

## Usage tutorial

A tutorial of how to install, upload and use this program:
1. Clone this repository's branch;
2. Connect pulse sending device's:
- Input to Nucleo D9 (output) slot;
- Output to Nucleo A0 (input) slot;
- GND to any Nucleo GND slot.
3. Upload this program to Nucleo board using VS Code with PlatformIO extension;

## Version 0.1v

This version must:
1. Init and configurate the needed hardware;
2. Receive the pulse signal from the sender device and calculate it's frequency;

## Version 0.2v

This version must:
1. Check if the last received pulse frequency is the same as the new one's;
2. Modify the pulse frequency according to specific formulas;
3. Send the the pulse with new frequency, but the same duty cycle, back to the source.