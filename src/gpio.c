/**
 * @file gpio.c
 * @brief Contains all functions bodies declared in gpio.h
 * 
 * @section DESCRIPTION
 * 
 * This file contains all functions for gpio configuration and 
 * initialisation.
 * 
 * @author Žygimantas Kungelis
 */ 


#include "gpio.h"


extern void GPIO_Init(void) 
{
    enable_GPIOx_Clocks();

    // LED gpio init
    clean_handler(&hled);
    init_GPIO_without_NVIC(&hled, &gpio_table[eGPIO_LD2]);

    // OC, PWM output pin init
    clean_handler(&hPWMoutput);
    init_GPIO_with_NVIC(&hPWMoutput,
                        &gpio_table[eGPIO_OC_PWM_TIM3],
                        TIM3_IRQn, 0, 0);
    
    // TIM2 NVIC init for interupt on rising edge
    clean_handler(&hTIM2gpio);
    init_GPIO_with_NVIC(&hTIM2gpio,
                        &gpio_table[eGPIO_IC_TIM2],
                        TIM2_IRQn, 0, 0);
}

static void enable_GPIOx_Clocks(void) {
    __HAL_RCC_GPIOH_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
}

static void clean_handler(void *specificHandler)
{
    memset(&specificHandler, 0, sizeof(*specificHandler));
}

static void init_GPIO_without_NVIC(
                        GPIO_InitTypeDef * const specificHandler, 
                        const struct GPIO * const table_value)
{
    init_specific_GPIO(specificHandler, table_value);
}

static void init_GPIO_with_NVIC(
                        GPIO_InitTypeDef * const specificHandler, 
                        const struct GPIO * const table_value,
                        const IRQn_Type IRQnHandlerName,
                        const uint32_t PreemtPriority,
                        const uint32_t SubPriority)
{
    init_specific_GPIO(specificHandler, table_value);
    HAL_NVIC_SetPriority(IRQnHandlerName, PreemtPriority, SubPriority);
    HAL_NVIC_EnableIRQ(IRQnHandlerName);
}

static void init_specific_GPIO(GPIO_InitTypeDef * const specificHandler, 
                               const struct GPIO * const table_value) {
    // table_value is read only 
    specificHandler->Pin = table_value->Pin;
    specificHandler->Mode = table_value->Mode;
    specificHandler->Alternate = table_value->Alternate;
    specificHandler->Pull = table_value->Pull;
    specificHandler->Speed = table_value->Speed;
    HAL_GPIO_Init(table_value->Port, specificHandler);
}
