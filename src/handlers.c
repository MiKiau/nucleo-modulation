/**
 * @file handlers.c
 * @brief Contains all functions bodies declared in handlers.h
 * 
 * @section DESCRIPTION
 * 
 * This file contains all handler functions' definitions.
 * 
 * @author Žygimantas Kungelis
 */ 


#include "handlers.h"


extern void Error_handler(void) 
{
    // If initialisation error occurred, stop the flow of the program
    while(1);
}

extern void TIM2_IRQHandler(void) {
    HAL_TIM_IRQHandler(&htim2);
}

extern void TIM3_IRQHandler(void) {
    HAL_TIM_IRQHandler(&htim3);
}

extern void SysTick_Handler(void) {
    // For Delay function
    HAL_IncTick();
}