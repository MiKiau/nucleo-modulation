/**
 * @file timers.c
 * @brief Contains all functions bodies declared in timers.h
 * 
 * @section DESCRIPTION
 * 
 * This file contains all functions' bodies for timers configuration and 
 * initialisation.
 * 
 * @author Žygimantas Kungelis
 */ 


#include "timers.h"


extern void SystemClock_Config(void) 
{
    RCC_OscInitTypeDef RCC_OscInitStruct = {0};
    RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

    /** 
     * Initializes the RCC Oscillators according to the specified parameters
     * in the RCC_OscInitTypeDef structure.
     */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
    RCC_OscInitStruct.MSIState = RCC_MSI_ON;
    RCC_OscInitStruct.MSICalibrationValue = 0;
    RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
    RCC_OscInitStruct.PLL.PLLM = 1;
    RCC_OscInitStruct.PLL.PLLN = 36;
    RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
    RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
    RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        Error_handler();
    }
    /** 
     * Initializes the CPU, AHB and APB buses clocks;
     * Select PLL as system clock source and configure the HCLK, PCLK1 and 
     * PCLK2 clocks dividers
     */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                                |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
    {
        Error_handler();
    }
    /** 
     * Configure the main internal regulator output voltage
     */
    if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) 
        != HAL_OK)
    {
        Error_handler();
    }
}

extern void TIM_Init(void) {

    // Enable timers
    enable_TIMx_clocks();

    // Init timer 2 as input counter
    clean_handler(&htim2);
    clean_handler(&inputCounterConfig);
    init_timer_as_IC(&htim2,
                     &timer_table[eInput_TIM2],
                     &inputCounterConfig,
                     &IC_channel_config[eINPUT1]);

    // Init timer 3 as output counter, PWM
    clean_handler(&htim3);
    clean_handler(&outputCounterConfig);
    init_timer_as_PWM(&htim3,
                      &timer_table[ePWM_TIM3],
                      &outputCounterConfig,
                      &OC_channel_config[eOUTPUT1]);

    // Init timer 16 as simple counter
    // init_specific_Timer(&htim16, &timer_table[eGeneric_TIM16]);
}

static void enable_TIMx_clocks(void) {
    __HAL_RCC_TIM2_CLK_ENABLE();
    __HAL_RCC_TIM3_CLK_ENABLE();
}

static void clean_handler(void *specificHandler)
{
    memset(specificHandler, 0, sizeof(*specificHandler));
}

static void init_timer_as_PWM(TIM_HandleTypeDef * const specificHandler,
                        const struct TIMER * const time_table_value,
                        TIM_OC_InitTypeDef * const OC_config,
                        const struct PULSE_OC_CONFIG * const OC_table_value) 
{
    init_specific_Timer(specificHandler, time_table_value);
    init_OC_channel(specificHandler, OC_config, OC_table_value);
}

static void init_timer_as_IC(TIM_HandleTypeDef * const specificHandler,
                        const struct TIMER * const time_table_value,
                        TIM_IC_InitTypeDef * const IC_config,
                        const struct PULSE_IC_CONFIG * const IC_table_value) 
{
    init_specific_Timer(specificHandler, time_table_value);
    init_IC_channel(specificHandler, IC_config, IC_table_value);
}

static void init_specific_Timer(TIM_HandleTypeDef * const specificHandler, 
                                const struct TIMER * const table_value) 
{
    // Basic clock config
    specificHandler->Instance = table_value->Instance;
    specificHandler->Init.Prescaler = table_value->Prescaler;
    specificHandler->Init.CounterMode = table_value->CounterMode;
    specificHandler->Init.Period = table_value->Period;
    specificHandler->Init.ClockDivision = table_value->ClockDivision;
    specificHandler->Init.AutoReloadPreload = table_value->AutoReloadPreload;
    if (HAL_TIM_Base_Init(specificHandler) != HAL_OK) {
        Error_handler();
    }
    // Clock source config
    TIM_ClockConfigTypeDef sClockSourceConfig = {0};
    sClockSourceConfig.ClockSource = table_value->ClockSource;
    if (HAL_TIM_ConfigClockSource(specificHandler, &sClockSourceConfig) != HAL_OK)
    {
        Error_handler();
    }
}

static void init_OC_channel(TIM_HandleTypeDef * const TIM_handler,
                            TIM_OC_InitTypeDef * const OC_config,
                            const struct PULSE_OC_CONFIG * const table_value) 
{
    // table_value is read only
    if (HAL_TIM_PWM_Init(TIM_handler) != HAL_OK) {
        Error_handler();
    }                           
    OC_config->OCMode = table_value->OCMode;
    OC_config->Pulse = table_value->Pulse;
    OC_config->OCPolarity = table_value->OCPolarity;
    OC_config->OCFastMode = table_value->OCFastMode;
    if (HAL_TIM_PWM_ConfigChannel(TIM_handler, OC_config, table_value->Channel)
        != HAL_OK) 
    {
        Error_handler();
    }
}

static void init_IC_channel(TIM_HandleTypeDef * const TIM_handler,
                            TIM_IC_InitTypeDef * const IC_config,
                            const struct PULSE_IC_CONFIG * const table_value) 
{
    if (HAL_TIM_IC_Init(TIM_handler) != HAL_OK)
    {
        Error_handler();
    }
    IC_config->ICPolarity = table_value->ICPolarity;
    IC_config->ICSelection = table_value->ICSelection;
    IC_config->ICPrescaler = table_value->ICPrescaler;
    IC_config->ICFilter = table_value->ICFilter;
    if (HAL_TIM_IC_ConfigChannel(TIM_handler, IC_config, table_value->Channel)
        != HAL_OK)
    {
        Error_handler();
    }
}