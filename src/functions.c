/**
 * @file functions.c
 * @brief Contains all functions bodies defined in functions.h
 * 
 * This file contains all functions for pulse receival, pulse modification.
 * 
 * @author Žygimantas Kungelis
 */ 

#include "functions.h"

extern double frequencyAccordingToTask(const int taskNumber, 
                                       const uint32_t frequency) {
    switch (taskNumber)
    {
    case 1:
        return frequency;
        break;
    case 2:
        return frequency + 50.0;
        break;
    case 3:
        return pow(frequency, 1.05);
        break;
    case 4:
        return frequency / M_PI_2;
        break;
    case 5:
        return frequency >> 1;
        break;
    case 6:
        return frequency / (((1 + pow(5, 0.5)) / 2) / 2);
        break;
    default:
        // Do nothing
        return frequency;
        break;
    }
}

extern void changeFrequencyAndDutyCycle(const uint32_t frequency, 
                                        const float dutyCycle)
{
    uint32_t ARRValue = System_Clock / (frequency * TIM3_Prescaler) - 1UL;
    uint32_t CCR2Value = ARRValue * dutyCycle / 100;
    TIM3->ARR = ARRValue;
    TIM3->CCR2 = CCR2Value;
}

extern uint32_t absForUint(const uint32_t a, const uint32_t b) {
    return (a >= b) ? a - b : b - a;
}

extern uint32_t mostFrequent(uint32_t *frequencyArray, int size) {
    qsort(frequencyArray, size, sizeof(frequencyArray[0]), comp);

    int maxCount = 1,
        currentCount = 1;
    uint32_t result = frequencyArray[0];
    for (int i = 1; i < size; i++) {
        if (frequencyArray[i] == frequencyArray[i - 1]) {
            currentCount++;
        } 
        else {
            if (currentCount > maxCount) {
                maxCount = currentCount;
                result = frequencyArray[i - 1];
            }
            currentCount = 1;
        }
    }
    if (currentCount > maxCount) {
        maxCount = currentCount;
        result = frequencyArray[size - 1];
    }
    return result;
}

static int comp(const void *elem1, const void *elem2) {
    uint32_t *x = (uint32_t *)elem1;
    uint32_t *y = (uint32_t *)elem2;
    return (*x > *y);
}