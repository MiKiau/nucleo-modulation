/**
 * @file main.c
 * @brief Nucleo L4 to Arduino Nano comunication.
 * 
 * @section DESCRIPTION
 * 
 * A program written for Nucleo STM32L476RG to do the following things:
 * 1. Receive a pulse signal and analyse it's pulse width and frequency;
 * 2. Modificate the pulse frequency according to specific formulas;
 * 3. Send the modificated pulse back to the source.
 * 
 * @section VERSION 0.2v
 * 
 * Version 0.2v must do these things:
 * 1. Check if the last received pulse frequency is the same as the new one's;
 * 2. Modify the pulse frequency according to specific formulas;
 * 3. Send the the pulse with new frequency, but the same duty cycle, back to 
 * the source.
 *  
 * @author Žygimantas Kungelis
 * @date 2020-12-03
 * @version 0.2 
 */ 

#include "main.h"

// Timer 3 clock after prescaler
#define F_CLK 2000000UL
#define MAX_TASK_NUMBER 6
// The number of elements to check for most frequent element
#define N 10

#define IDLE 0
#define DONE 1

#define false 0
#define true 1

// Variables used in IC_CaptureCallback
static volatile uint8_t inputState = IDLE;
static volatile uint32_t T1 = 0UL;
static volatile uint32_t T2 = 0UL;
static volatile uint32_t ticks = 0UL;
static volatile uint32_t TIM2_overCount = 0UL;
static volatile uint32_t inputFrequency = 0UL;

// Other variables
static uint32_t frequencyArray[N];
static int frequencyCount = 0;
static uint32_t currentFrequency = 0UL;
static uint32_t oldFrequency = 0UL;
static uint32_t dutyCycle = 50UL;
static int taskNumber = 1;

// Flags
static volatile uint8_t inputFrequencyCalculated = false;
static volatile uint8_t changeOutputFrequency = false;


int main(void) 
{
    HAL_Init();

    SystemClock_Config();
    GPIO_Init();
    TIM_Init();

    // Needed for enabling PeriodElapsedCallback
    __HAL_TIM_ENABLE_IT(&htim2, TIM_IT_UPDATE);

    // Start receiving pulses
    HAL_TIM_IC_Start_IT(&htim2, TIM_INPUT_CHANNEL);

    // Start sending pulses
    HAL_TIM_PWM_Start_IT(&htim3, TIM_OUTPUT_CHANNEL);

    while (1) {
        if (inputFrequencyCalculated) {
            inputFrequencyCalculated= false;
            
            frequencyArray[frequencyCount++] = inputFrequency;

            if (frequencyCount == N - 1) {
                frequencyCount = 0;
                // Check the last N-1 elements to find out which is the most
                // frequent element
                currentFrequency = mostFrequent(frequencyArray, N);
                if (oldFrequency == 0UL) {
                    oldFrequency = currentFrequency;
                    // Update output frequency
                    changeOutputFrequency = true;
                }
                else if (absForUint(oldFrequency, currentFrequency) > 5UL) {
                    // If the new frequency differs by 5 from the old one,
                    // that means a new task has started
                    if (taskNumber < MAX_TASK_NUMBER) {
                        taskNumber++;
                    }
                    oldFrequency = currentFrequency;
                    // Update output frequency
                    changeOutputFrequency = true;
                }
            }
        }

        if (changeOutputFrequency) {
            changeOutputFrequency = false;
            // Calculate the specific task frequency according to input freq.
            uint32_t newFrequency = (uint32_t)frequencyAccordingToTask(
                                                            taskNumber, 
                                                            currentFrequency);
            
            changeFrequencyAndDutyCycle(newFrequency, dutyCycle);
        }
    }
}

void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim) 
{
    if (htim == &htim2) {
        if (inputState == IDLE) {
            // First rise of a pulse edge
            // Save current timer's count
            T1 = TIM2->CCR1;
            // Start counting how many times will the counter overflow
            TIM2_overCount = 0;
            inputState = DONE;
        }
        else if (inputState == DONE) {
            // Second rise of a pulse edge
            // Save current timer's count
            T2 = TIM2->CCR1;
            // Calculate how many ticks does a single pulse period contain 
            ticks = (T2 + (TIM2_overCount * 65536)) - T1;
            // Calculate frequency
            inputFrequency = (uint32_t)(F_CLK/ticks);
            inputFrequencyCalculated = true;
            inputState = IDLE;
        }
    }
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) 
{
    if (htim->Instance == TIM2) {
        TIM2_overCount++;
    }
}